function NhanVien(tknv, name, email, password, date, luongCB, chucvu, gioLam) {
  this.tknv = tknv;
  this.name = name;
  this.email = email;
  this.password = password;
  this.date = date;
  this.luongCB = luongCB;
  this.chucvu = chucvu;
  this.gioLam = gioLam;
  this.tongLuong = function (chucvu) {
    if (this.chucvu == "Sếp") {
      return this.luongCB * 3;
    } else if (this.chucvu == "Trưởng phòng") {
      return this.luongCB * 2;
    } else if (this.chucvu == "Nhân viên") {
      return this.luongCB;
    }
  };
  this.xepLoai = function (gioLam) {
    if (this.gioLam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
