function renderList(dsnv) {
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    var content = `
        <tr>
            <td>${nv.tknv}</td>
            <td>${nv.name}</td>
            <td>${nv.email}</td>
            <td>${nv.date}</td>
            <td>${nv.chucvu}</td>
            <td>${nv.tongLuong()}</td>
            <td>${nv.xepLoai()}</td>
            <td>
                <button onclick ="xoaNhanVien('${
                  nv.tknv
                }')" class="btn btn-danger">Xóa</button>
                <button onclick ="suaNhanVien('${
                  nv.tknv
                }')"class="btn btn-warning">Sửa</button>
            </td>
        </tr>`;
    contentHTML += content;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var tknv = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var date = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucvu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;
  //tạo sv

  return new NhanVien(
    tknv,
    name,
    email,
    password,
    date,
    luongCB,
    chucvu,
    gioLam
  );
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.tknv;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.date;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.chucvu;
  document.getElementById("gioLam").value = nv.gioLam;
}
