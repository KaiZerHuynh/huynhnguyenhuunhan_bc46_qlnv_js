function showMessage(idValue, message) {
  document.getElementById(idValue).innerText = message;
}

function kiemTraRong(idValue, value) {
  if (value == "") {
    showMessage(idValue, "Thông tin không được để trống");
    return false;
  } else {
    showMessage(idValue, "");
    return true;
  }
}

function kiemTraDoDai(min, max, idValue, message, value) {
  var length = value.length;
  if (length >= min && length <= max) {
    showMessage(idValue, "");
    return true;
  } else {
    showMessage(idValue, message);
    return false;
  }
}

function kiemTraTen(name) {
  if (/^[a-zA-Z ]+$/.test(name)) {
    showMessage("tbName", "");
    return true;
  } else {
    showMessage("tbName", "Tên không hợp lệ");
    return false;
  }
}

function kiemTraEmail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(email);
  if (isEmail) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ");
    return false;
  }
}

function kiemTraKyTu(pass) {
  const re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/i;
  var isPass = re.test(pass);
  if (isPass) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu cần chứa ít nhất  1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    );
    return false;
  }
}

function kiemTraNgay(date) {
  const re = /^\d{2}\/\d{2}\/\d{4}$/i;
  var isPass = re.test(date);
  if (isPass) {
    showMessage("tbNgay", "");
    return true;
  } else {
    showMessage("tbNgay", "Bạn cần điền đúng định dạng mm/dd/yyyy");
    return false;
  }
}

function kiemTraLuong(money) {
  if (money >= 1000000 && money <= 20000000) {
    showMessage("tbLuongCB", "");
    return true;
  } else {
    showMessage("tbLuongCB", "Mức lương của bạn chưa phù hợp");
    return false;
  }
}

function kiemTraChucVu(chucvu) {
  if (chucvu == "Sếp" || chucvu == "Trưởng phòng" || chucvu == "Nhân viên") {
    showMessage("tbChucVu", "");
    return true;
  } else {
    showMessage("tbChucVu", "Chức vụ không hợp lệ");
    return false;
  }
}

function kiemTraGioLam(time) {
  if (time >= 80 && time <= 200) {
    showMessage("tbGiolam", "");
    return true;
  } else {
    showMessage("tbGiolam", "Giờ làm của bạn chưa đúng");
    return false;
  }
}
