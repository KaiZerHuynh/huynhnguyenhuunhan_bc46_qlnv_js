var dsnv = [];

var dataJson = localStorage.getItem("DSNV");
if (dataJson != null) {
  dsnv = JSON.parse(dataJson).map(function (item) {
    return new NhanVien(
      item.tknv,
      item.name,
      item.email,
      item.password,
      item.date,
      item.luongCB,
      item.chucvu,
      item.gioLam
    );
  });
  //map
  renderList(dsnv);
}
function themNhanVien() {
  var nv = layThongTinTuForm();

  //validate
  //Kiểm tra tài khoản
  var isValid =
    kiemTraRong("tbTKNV", nv.tknv) &&
    kiemTraDoDai(4, 6, "tbTKNV", "Tài khoản có độ dài 4 đến 6", nv.tknv);
  //Kiểm tra tên
  isValid = isValid & (kiemTraRong("tbName", nv.name) && kiemTraTen(nv.name));
  //Kiểm tra email
  isValid =
    isValid & (kiemTraRong("tbEmail", nv.email) && kiemTraEmail(nv.email));
  //Kiểm tra password
  isValid =
    isValid &
    (kiemTraRong("tbMatKhau", nv.password) &&
      kiemTraDoDai(
        6,
        10,
        "tbMatKhau",
        "Mật khẩu phải có ít nhất 6 đến 10 từ",
        nv.password
      ) &&
      kiemTraKyTu(nv.password));
  //Kiểm tra ngày tháng năm
  isValid = isValid & (kiemTraRong("tbNgay", nv.date) && kiemTraNgay(nv.date));
  //Kiểm tra lương
  isValid =
    isValid &
    (kiemTraRong("tbLuongCB", nv.luongCB) && kiemTraLuong(nv.luongCB));
  //Kiểm tra chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucvu);
  //Kiểm tra giờ làm
  isValid =
    isValid & (kiemTraRong("tbGiolam", nv.gioLam) && kiemTraGioLam(nv.gioLam));
  if (isValid) {
    $("#myModal").modal("hide");
    dsnv.push(nv);

    //render dssv: đưa data lên giao diện
    renderList(dsnv);

    //save dssv localStorage
    //localStorage : nơi lưu trữ (chỉ chấp nhận json), json: 1 loại dữ liệu
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
  }
}

function xoaNhanVien(id) {
  var index = dsnv.findIndex(function (item) {
    return item.tknv == id;
  });
  dsnv.splice(index, 1);
  renderList(dsnv);
}

function suaNhanVien(id) {
  $("#myModal").modal("show");
  var index = dsnv.findIndex(function (item) {
    return item.tknv == id;
  });
  //show thong tin len form
  showThongTinLenForm(dsnv[index]);
}

function capNhat() {
  var nv = layThongTinTuForm();
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i].tknv == nv.tknv) {
      dsnv[i].name = nv.name;
      dsnv[i].email = nv.email;
      dsnv[i].password = nv.password;
      dsnv[i].date = nv.date;
      dsnv[i].luongCB = nv.luongCB;
      dsnv[i].chucvu = nv.chucvu;
      dsnv[i].gioLam = nv.gioLam;
      //DOM hiển thị dsnv
      document.getElementById("name").value = dsnv[i].name;
      document.getElementById("email").value = dsnv[i].email;
      document.getElementById("password").value = dsnv[i].password;
      document.getElementById("datepicker").value = dsnv[i].date;
      document.getElementById("luongCB").value = dsnv[i].luongCB;
      document.getElementById("chucvu").value = dsnv[i].chucvu;
      document.getElementById("gioLam").value = dsnv[i].gioLam;
    }
  }
  var isValid =
    kiemTraRong("tbTKNV", nv.tknv) &&
    kiemTraDoDai(4, 6, "tbTKNV", "Tài khoản có độ dài 4 đến 6", nv.tknv);
  //Kiểm tra tên
  isValid = isValid & (kiemTraRong("tbName", nv.name) && kiemTraTen(nv.name));
  //Kiểm tra email
  isValid =
    isValid & (kiemTraRong("tbEmail", nv.email) && kiemTraEmail(nv.email));
  //Kiểm tra password
  isValid =
    isValid &
    (kiemTraRong("tbMatKhau", nv.password) &&
      kiemTraDoDai(
        6,
        10,
        "tbMatKhau",
        "Mật khẩu phải có ít nhất 6 đến 10 từ",
        nv.password
      ) &&
      kiemTraKyTu(nv.password));
  //Kiểm tra ngày tháng năm
  isValid = isValid & (kiemTraRong("tbNgay", nv.date) && kiemTraNgay(nv.date));
  //Kiểm tra lương
  isValid =
    isValid &
    (kiemTraRong("tbLuongCB", nv.luongCB) && kiemTraLuong(nv.luongCB));
  //Kiểm tra chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucvu);
  //Kiểm tra giờ làm
  isValid =
    isValid & (kiemTraRong("tbGiolam", nv.gioLam) && kiemTraGioLam(nv.gioLam));
  if (isValid) {
    $("#myModal").modal("hide");
    renderList(dsnv);
    //lưu trữ trong dataStorage
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
  }
}

function findNhanVien() {
  var grade = document.getElementById("searchName").value;
  var dsnvTim = dsnv.filter(function (item) {
    console.log(item.xepLoai().toString().toLowerCase());
    return (
      item
        .xepLoai()
        .toString()
        .toLowerCase()
        .indexOf(grade.toString().toLowerCase()) !== -1
    );
  });
  renderList(dsnvTim);
}
